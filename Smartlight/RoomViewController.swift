//
//  HomeViewController.swift
//  Smartlight
//
//  Created by Ahmad Akhmiev on 15/11/15.
//  Copyright © 2015 Ahmad Akhmiev. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import CoreData
import CoreBluetooth

class RoomViewController: UIViewController {
    
    @IBOutlet weak var selectedCellLabel: UINavigationItem!
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var lightSwitchButton: UIButton!
    @IBOutlet weak var brigthnessSlider: UISlider!
    @IBOutlet weak var brightnessText: UILabel!
    
    var rooms = [String: String]()
    var menuView: BTNavigationDropdownMenu!
    let httpHelper = HTTPHelper()
    var lightsOn: Bool = true
    var lightSettings = [LightSetting]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicatorView.hidden = false
        self.activityIndicatorView.layer.cornerRadius = 10
        
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green:180/255.0, blue:220/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        drawBrightnessText()
        loadRooms()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawBrightnessText() {
        brightnessText.text = String(Int(brigthnessSlider.value*100)) + " %"
        brightnessText.textColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
    }
    
    func drawDropDownMenu() {
        if rooms.isEmpty {
            self.selectedCellLabel.title = "No rooms available"
            displayAlertMessage("Could not load rooms", alertDescription: "Make sure bluetooth is activated and you are close to a beacon")
            tryAgainButton.hidden = false
            return
        }
        
        self.selectedCellLabel.title = self.rooms.keys.first
        
        let menuView = BTNavigationDropdownMenu(title: rooms.keys.first!, items: Array(rooms.keys))
        
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.cellTextLabelColor = UIColor.whiteColor()
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.blackColor()
        menuView.maskBackgroundOpacity = 0.3
        
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
            let selectedRoomKey = Array(self.rooms.keys)[indexPath]
            self.selectedCellLabel.title = selectedRoomKey
            self.refreshRoomControls(Array(self.rooms.values)[indexPath])
        }
        self.navigationItem.titleView = menuView
        
        refreshRoomControls(self.rooms.values.first)
    }
    
    //TO BE INCLUDED IN POST
    func loadRooms() {
        // Create HTTP request and set request Body
        let httpRequest = httpHelper.buildRequest("offices/all", method: "GET", authType: nil, requestContentType: .HTTPJsonContent)
        
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                self.displayAlertMessage("Error", alertDescription: errorMessage as String)
                self.activityIndicatorView.hidden = true
                
                return
            }
            
            self.activityIndicatorView.hidden = true
            
            let responseDict = try!NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            
            if let offices: AnyObject = responseDict["offices"]{
                for aOffice in offices as! NSArray {
                    var friendlyName = ""
                    var path = ""
                    for aKey in (aOffice as! NSDictionary).allKeys{
                        //print(aKey)
                        if aKey as! String == "friendlyName" {
                            friendlyName = aOffice.objectForKey(aKey) as! String
                        }
                        if aKey as! String == "path" {
                            path = aOffice.objectForKey(aKey) as! String
                        }
                    }
                    self.rooms[friendlyName] = path
                }
            }
            self.drawDropDownMenu()
            self.setRoomPathOnSettingsView()
        })
    }
    
    func displayAlertMessage(alertTitle:String, alertDescription:String) -> Void {
        // hide activityIndicator view and display alert message
        self.activityIndicatorView.hidden = true
        let errorAlert = UIAlertController(title: alertTitle, message: alertDescription, preferredStyle: UIAlertControllerStyle.Alert)
        errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {
            (action: UIAlertAction!) in
            errorAlert.dismissViewControllerAnimated(true, completion: nil)
        }))
        presentViewController(errorAlert, animated: true, completion: nil)
    }
    
    func refreshRoomControls(roomPath:String!) {
        brigthnessSlider.value = 0.5
        brightnessText.text = "50 %"
        
        let url = "controls/latestReading?officePath="+roomPath
        
        let httpRequest = httpHelper.buildRequest(url, method: "GET",
            authType: nil, requestContentType: nil)
        
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                self.displayAlertMessage("Error", alertDescription: errorMessage as String)
                return
            }
            
            let responseDict = try!NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            
            if let readings = responseDict["Readings"]{
                //var timestamp: Int64
                var state = 0
                if readings is NSNull {
                    self.refreshRoomControls(roomPath)
                    return
                }
                for aReading in readings as! NSArray {
                    //timestamp = aReading[0] as! Int64
                    state = aReading[1] as! Int
                }
                if state == 1 {
                    self.lightsOn = true
                }
                else {
                    self.lightsOn = false
                }
                self.drawRoomControls()
                self.setRoomPathOnSettingsView()
                
            }
        })
    }
    
    func drawRoomControls() {
        switchLights()
    }
    
    //TO BE INCLUDED IN POST
    @IBAction func lightSwitchClicked(sender: AnyObject) {
        let roomPath = self.rooms[selectedCellLabel.title!]
        
        var state = 1
        
        if lightsOn {
            state = 0
        }
        
        let url = "controls/toggleActuator?officePath="+roomPath!+"&state="+String(state)
        
        let httpRequest = httpHelper.buildRequest(url, method: "GET",
            authType: nil, requestContentType: nil)
        
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                self.displayAlertMessage("Error", alertDescription: errorMessage as String)
                return
            }
            
            _ = try!NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            
            self.lightsOn = Bool(state)
            self.switchLights()
        })
    }
    
    //TO BE INCLUDED IN POST
    func switchLights() {
        if(lightsOn) {
            lightSwitchButton.setImage(UIImage(named: "power_on.png"), forState: .Normal)
            brigthnessSlider.enabled = true
        }
        else {
            lightSwitchButton.setImage(UIImage(named: "power_off.png"), forState: .Normal)
            brigthnessSlider.enabled = false
        }
    }

    @IBAction func brightnessChanged(sender: AnyObject) {
        let thisSlider = sender as! UISlider
        let currentValue = Int(thisSlider.value*100)
        
        brightnessText.text = "\(currentValue) %"
        print(currentValue)
    }
    
    func setRoomPathOnSettingsView() {
        let barControllers = self.tabBarController?.viewControllers
        let navigationController = barControllers![1] as!  UINavigationController
        let settingsViewController = navigationController.viewControllers[0] as! SettingsViewController
        settingsViewController.roomPath = self.rooms[self.selectedCellLabel.title!]!
    }
    
    @IBAction func saveSettings(sender: AnyObject) {
        let alert = UIAlertController(title: "New setting", message: "Add a new name", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .Default, handler: { (action:UIAlertAction) -> Void in
            let textField = alert.textFields!.first
            self.saveSetting(textField!.text!, roomPath: self.rooms[self.selectedCellLabel.title!]!, lightVolume: self.brigthnessSlider.value, lightsOn: self.lightsOn, roomFriendlyName: self.selectedCellLabel.title!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
            style: .Default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (textField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func saveSetting(name: String, roomPath: String, lightVolume: Float, lightsOn: Bool, roomFriendlyName: String) {
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("LightSetting", inManagedObjectContext:managedContext)
        let setting = LightSetting(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        //3
        setting.setValue(name, forKey: "name")
        setting.setValue(roomPath, forKey: "roomPath")
        setting.setValue(lightVolume, forKey: "lightVolume")
        setting.setValue(roomFriendlyName, forKey: "roomFriendlyName")
        if(lightsOn) {
            setting.setValue("ON", forKey: "lightState")
        }
        else {
            setting.setValue("OFF", forKey: "lightState")
        }
        
        //4
        do {
            try managedContext.save()
            //5
            lightSettings.append(setting)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
