//
//  HTTPHelper
//  Semply
//
//  Created by Ahmad Akhmiev on 08/10/15.
//  Copyright © 2015 Ahmad Akhmiev. All rights reserved.
//

import Foundation

enum HTTPRequestAuthType {
  case HTTPBasicAuth
  case HTTPTokenAuth
}

enum HTTPRequestContentType {
  case HTTPJsonContent
  case HTTPMultipartContent
}

struct HTTPHelper {
    static let BASE_URL = "http://localhost:9000"
    
    func buildRequest(path: String!, method: String, authType: HTTPRequestAuthType?,
        requestContentType: HTTPRequestContentType?, requestBoundary:String = "") -> NSMutableURLRequest {
            
            let requestURL = NSURL(string: "\(HTTPHelper.BASE_URL)/\(path)")
            let request = NSMutableURLRequest(URL: requestURL!)

            request.HTTPMethod = method
            
            if let reqContentType = requestContentType {
                switch reqContentType {
                    case .HTTPJsonContent:
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    case .HTTPMultipartContent:
                        let contentType = "multipart/form-data; boundary=\(requestBoundary)"
                        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
                }
            }
            
            if authType == HTTPRequestAuthType.HTTPTokenAuth {
                if let userToken = KeychainAccess.passwordForAccount("token", service: "KeyChainService") as String? {
                   request.addValue("\(userToken)", forHTTPHeaderField: "Authorization")
                }
            }
      
            return request
  }
  
  func sendRequest(request: NSURLRequest, completion:(NSData!, NSError!) -> Void) -> () {
    
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithRequest(request) {
        (data:NSData?, response:NSURLResponse?, error:NSError?) in
        
      if error != nil {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          completion(data, error)
        })
        
        return
      }
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        if let httpResponse = response as? NSHTTPURLResponse {
          if httpResponse.statusCode == 200 {
            completion(data, nil)
          } else {
            if let errorDict = try!NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
              let responseError : NSError = NSError(domain: "HTTPHelperError",
                code: httpResponse.statusCode, userInfo: errorDict as [NSObject : AnyObject])
              completion(data, responseError)
            }
          }
        }
      })
    }
    
    task.resume()
  }
  
  func getErrorMessage(error: NSError) -> NSString {
    var errorMessage : NSString
    
    // return correct error message
    if error.domain == "HTTPHelperError" {
      let userInfo = error.userInfo as NSDictionary!
      errorMessage = userInfo.valueForKey("exception") as! NSString
    } else {
      errorMessage = error.description
    }
    
    return errorMessage
  }
}
