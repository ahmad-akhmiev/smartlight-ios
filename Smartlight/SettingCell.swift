//
//  SettingCell.swift
//  Smartlight
//
//  Created by Ahmad Akhmiev on 16/11/15.
//  Copyright © 2015 Ahmad Akhmiev. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var applySettingButton: UIButton!
    
    var setting: LightSetting! {
        didSet {
            descriptionLabel.text = setting.roomFriendlyName
            titleLabel.text = setting.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
