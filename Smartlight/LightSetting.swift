//
//  LightSetting.swift
//  Smartlight
//
//  Created by Ahmad Akhmiev on 16/11/15.
//  Copyright © 2015 Ahmad Akhmiev. All rights reserved.
//

import Foundation
import CoreData

public class LightSetting: NSManagedObject {
    @NSManaged public var name: String?
    @NSManaged public var roomPath: String?
    @NSManaged public var lightVolume: NSDecimalNumber?
    @NSManaged public var lightState: String?
    @NSManaged public var roomFriendlyName: String?
    
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

}